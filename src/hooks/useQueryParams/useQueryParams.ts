import { useEffect } from 'react';
import { NumberParam, StringParam, useQueryParams as useParams } from 'use-query-params';

import { RESULTS_PER_PAGE_COUNT } from '../../app.constants';

import { IQueryParams, OrderType, SortType } from './useQueryParams.types';

export const filterQuery = (
  { sort, page, order, perPage }: IQueryParams,
  callback: (obj: Partial<IQueryParams>) => void
) => {
  const queryObject = {
    sort: Object.values(SortType).includes(sort as SortType) ? sort : undefined,
    page: page && (page < 1 || page > 100) ? 1 : page,
    order: Object.values(OrderType).includes(order as OrderType) ? order : undefined,
    perPage: perPage && perPage !== RESULTS_PER_PAGE_COUNT ? RESULTS_PER_PAGE_COUNT : perPage,
  };

  callback(queryObject);
};

export const useQueryParams = () => {
  const [query, setQuery] = useParams({
    q: StringParam,
    sort: StringParam,
    order: StringParam,
    perPage: NumberParam,
    page: NumberParam,
  });

  useEffect(() => {
    filterQuery(query, setQuery);
  }, [query]);

  const setNewSearchQuery = (value: string) => {
    setQuery({
      q: value,
      sort: undefined,
      order: undefined,
      perPage: RESULTS_PER_PAGE_COUNT,
      page: 1,
    });
  };

  return {
    query,
    setQuery,
    setNewSearchQuery,
  };
};
