export enum OrderType {
  ASC = 'asc',
  DESC = 'desc',
}

export enum SortType {
  STARS = 'stars',
  NAME = 'name',
}

export type StringQueryType = string | null | undefined;

export type StringNumberType = number | null | undefined;

export interface IQueryParams {
  q: StringQueryType;
  sort: StringQueryType;
  order: StringQueryType;
  perPage: StringNumberType;
  page: StringNumberType;
}
