import React from 'react';
import { Helmet } from 'react-helmet';

import { Container } from './app.styles';
import { Background } from './components/background';
import { Repositories } from './modules/repositories';

const App = () => {
  return (
    <Container>
      <Helmet>
        <title>Fourthwall Task</title>
        <meta name="description" content="Fourthwall Task" />
      </Helmet>

      <Background />
      <Repositories />
    </Container>
  );
};

export default App;
