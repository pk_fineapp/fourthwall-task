import { createGlobalStyle } from 'styled-components';

import { COLORS } from './colors';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background: ${COLORS.WHITE};
    font-family: Arial, Helvetica, Sans-Serif;
    height: 100vh;
    overflow: hidden;
  }
  
  button {
    background: none;
    border: none;
    
    &:focus {
      outline: none;
    }
  }
`;

export default GlobalStyle;
