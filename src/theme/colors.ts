export enum COLORS {
  WHITE = '#fff',
  BLUE = '#0C49F5',
  BLUE_HOVER = '#0F3CB7',
  BLUE_ACTIVE = '#0A2671',
  BLACK = '#000',
  GRAY = '#757575',
  GRAY_HOVER = '#505050',
  GRAY_LIGHT = '#F4F4F4',
}
