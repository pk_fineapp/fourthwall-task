export const MOBILE = 'mobile';
export const TABLET = 'tablet';
export const DESKTOP = 'desktop';

export type BreakpointType = 'mobile' | 'tablet' | 'desktop';

export const breakpoints = {
  mobile: 576,
  tablet: 768,
  desktop: 994,
};

export const heightBreakpoints = {
  tablet: 576,
};

const size = {
  mobile: `${breakpoints.mobile}px`,
  tablet: `${breakpoints.tablet}px`,
  desktop: `${breakpoints.desktop}px`,
  tabletHeight: `${heightBreakpoints.tablet}px`,
};

export const media = {
  mobile: `@media (min-width: ${size.mobile})`,
  tablet: `@media (min-width: ${size.tablet})`,
  tabletHeight: `@media (min-width: ${size.tablet}) and (min-height: ${size.tabletHeight})`,
  desktop: `@media (min-width: ${size.desktop})`,
};
