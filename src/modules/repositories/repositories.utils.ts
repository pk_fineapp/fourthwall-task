import { RESULTS_PER_PAGE_COUNT, MAX_PAGES } from '../../app.constants';
import { OrderType, SortType } from '../../hooks/useQueryParams/useQueryParams.types';

export const getParamsFromSelect = (value: unknown): [SortType | undefined, OrderType | undefined] => {
  let sortValue;
  let orderValue;

  if (typeof value === 'string') {
    if (value.toLowerCase().includes(SortType.NAME)) {
      sortValue = SortType.NAME;
    }

    if (value.toLowerCase().includes(SortType.STARS)) {
      sortValue = SortType.STARS;
    }

    if (value.toLowerCase().includes('ascending') || value.toLowerCase().includes('a-z')) {
      orderValue = OrderType.ASC;
    }
  }

  return [sortValue, orderValue];
};

export const getMaxPagesCount = (totalCount: number) => {
  return totalCount > RESULTS_PER_PAGE_COUNT * MAX_PAGES ? MAX_PAGES : Math.ceil(totalCount / RESULTS_PER_PAGE_COUNT);
};
