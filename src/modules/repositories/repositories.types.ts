import { QueryStatus } from 'react-query';

export interface IOwner {
  login: string;
}

export interface IRepositoriesItem {
  id: number;
  name: string;
  owner: IOwner;
  stargazersCount: number;
  createdAt: Date;
}

export interface IRepositories {
  items: IRepositoriesItem[];
  totalCount: number;
}
export interface IRepositoriesComponent {
  inputValue: string;
  changeInputValue: (v: string) => void;
  searchSubmit: () => void;
  repositories: IRepositories | undefined;
  status: QueryStatus;
  sortBy: SortByType;
  changeSortByType: (v: unknown) => void;
  page: number;
  pagesCount: number;
  changePage: (p: number) => void;
}

export enum SortByType {
  DEFAULT = 'Best match',
  'NAME_DESC' = 'Name Z-A',
  'NAME_ASC' = 'Name A-Z',
  'STARS_DESC' = 'Stars descending',
  'STARS_ASC' = 'Stars ascending',
}
