import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { RepositoriesTable } from '../repositoriesTable';

import { Container, useStyles, Text } from './repositoriesContent.styles';
import { IRepositoriesContent } from './repositoriesContent.types';

export const RepositoriesContent = ({
  repositories,
  status,
  sortBy,
  changeSortByType,
  page,
  pagesCount,
  changePage,
}: IRepositoriesContent) => {
  const classes = useStyles();

  const renderIdleText = () => <Text>Type a repository name and click search to get results.</Text>;

  const renderNoResultsText = () => <Text>Unfortunately, there are no repositories found.</Text>;

  const renderError = () => <Text>Something went wrong. Please, try again.</Text>;

  const renderTable = () => (
    <RepositoriesTable
      repositories={repositories?.items || []}
      sortBy={sortBy}
      changeSortByType={changeSortByType}
      page={page}
      pagesCount={pagesCount}
      changePage={changePage}
    />
  );

  const renderLoader = () => <CircularProgress data-testid="loader" className={classes.loader} />;

  const renderSuccessContent = () => {
    if (repositories?.totalCount && repositories.items) {
      return renderTable();
    } else {
      return renderNoResultsText();
    }
  };

  const renderContent = () => {
    switch (status) {
      case 'idle': {
        return renderIdleText();
      }
      case 'success': {
        return renderSuccessContent();
      }
      case 'loading': {
        return renderLoader();
      }
      case 'error': {
        return renderError();
      }
      default: {
        return renderError();
      }
    }
  };

  return <Container>{renderContent()}</Container>;
};
