import { QueryStatus } from 'react-query';

import {IRepositories, SortByType} from '../../repositories.types';

export interface IRepositoriesContent {
  repositories: IRepositories | undefined;
  status: QueryStatus;
  sortBy: SortByType;
  changeSortByType: (v: unknown) => void;
  page: number;
  pagesCount: number;
  changePage: (p: number) => void;
}