import styled from 'styled-components';
import { makeStyles } from '@material-ui/core';

import {COLORS} from '../../../../theme/colors';


export const useStyles = makeStyles({
  loader: {
    margin: '60px 0',
    color: COLORS.BLUE,
  },
});

export const Container = styled.div`
  height: 100%;
  width: 640px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Text = styled.p`
  height: fit-content;
  padding: 0;
  font-size: 13px;
  margin-top: 60px;
  color: ${COLORS.GRAY};
`;
