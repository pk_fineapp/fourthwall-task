import { IRepositoriesItem, SortByType } from '../../repositories.types';

export interface IRepositoriesTable {
  repositories: IRepositoriesItem[];
  sortBy: SortByType;
  changeSortByType: (v: unknown) => void;
  page: number;
  pagesCount: number;
  changePage: (p: number) => void;
}
