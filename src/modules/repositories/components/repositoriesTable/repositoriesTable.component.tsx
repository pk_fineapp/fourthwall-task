import React from 'react';

import { repositoriesTableColumns } from '../../repositories.constants';
import { Table } from '../../../../components/table';
import { RepositoriesTableBody } from '../repositoriesTableBody';
import { Select } from '../../../../components/select';
import { SortByType } from '../../repositories.types';
import { Pagination } from '../../../../components/pagination';

import { IRepositoriesTable } from './repositoriesTable.types';
import { Container } from './repositoriesTable.styles';
import { InterfaceContainer } from './interfaceContainer';

export const RepositoriesTable = ({
  repositories,
  sortBy,
  changeSortByType,
  page,
  pagesCount,
  changePage,
}: IRepositoriesTable) => {
  return (
    <Container>
      <InterfaceContainer>
        <Select id="sort-select" value={sortBy} items={Object.values(SortByType)} onChange={changeSortByType} />
      </InterfaceContainer>
      <Table columns={repositoriesTableColumns} body={<RepositoriesTableBody body={repositories} />} />
      <InterfaceContainer>
        <Pagination id="repositories-pagination" count={pagesCount} page={page} onChange={changePage} />
      </InterfaceContainer>
    </Container>
  );
};
