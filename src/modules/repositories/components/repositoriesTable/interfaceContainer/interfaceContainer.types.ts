import { ReactNode } from 'react';

export interface IInterfaceContainer {
  children?: ReactNode;
}
