import React from 'react';

import { IInterfaceContainer } from './interfaceContainer.types';
import { Container } from './interfaceContainer.styles';

export const InterfaceContainer = ({ children }: IInterfaceContainer) => {
  return <Container>{children}</Container>;
};
