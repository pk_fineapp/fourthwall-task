import React from 'react';
import dayjs from 'dayjs';

import { TableBody } from '../../../../components/table/tableBody';
import { IRepositoriesItem } from '../../repositories.types';
import { TableRow } from '../../../../components/table/tableRow';
import { TableCell } from '../../../../components/table/tableCell';

import { IRepositoriesTableBody } from './repositoriesTableBody.types';

export const RepositoriesTableBody = ({ body }: IRepositoriesTableBody) => {
  if (!body) {
    return null;
  }

  return (
    <TableBody>
      {body.map((item: IRepositoriesItem) => (
        <TableRow key={item.id}>
          <TableCell value={item.name} />
          <TableCell value={item.owner.login} />
          <TableCell value={item.stargazersCount.toString()} />
          <TableCell value={dayjs(item.createdAt).format('YYYY-MM-DD HH:mm')} />
        </TableRow>
      ))}
    </TableBody>
  );
};
