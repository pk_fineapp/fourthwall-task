import { IRepositoriesItem } from '../../repositories.types';

export interface IRepositoriesTableBody {
  body: IRepositoriesItem[] | undefined;
}
