import React, { useEffect, useState } from 'react';

import { useQueryParams } from '../../hooks/useQueryParams/useQueryParams';

import { useRepositoriesQuery } from './hooks/useRepositoriesQuery/useRepositoriesQuery';
import { RepositoriesComponent } from './repositories.component';
import { SortByType } from './repositories.types';
import { getMaxPagesCount, getParamsFromSelect } from './repositories.utils';

export const RepositoriesContainer = () => {
  const { query, setQuery, setNewSearchQuery } = useQueryParams();
  const [inputValue, setInputValue] = useState<string>(query?.q || '');
  const [sortBy, setSortBy] = useState<SortByType>(SortByType.DEFAULT);
  const [page, setPage] = useState(1);
  const {
    repositories,
    repositoriesQuery: { status },
  } = useRepositoriesQuery();
  const maxPagesCount = repositories?.totalCount ? getMaxPagesCount(repositories.totalCount) : 0;

  const changeInputValue = (value: string) => setInputValue(value);

  const searchSubmit = () => {
    if (inputValue?.length) {
      setSortBy(SortByType.DEFAULT);
      setNewSearchQuery(inputValue);
    }
  };

  const changeSortByType = (value: unknown) => setSortBy(value as SortByType);

  const changePage = (page: number) => setPage(page);

  useEffect(() => {
    const [sortValue, orderValue] = getParamsFromSelect(sortBy);

    setQuery({ sort: sortValue || undefined, order: orderValue || undefined });
  }, [sortBy]);

  useEffect(() => {
    setQuery({ page: page === 1 ? undefined : page });
  }, [page]);

  return (
    <RepositoriesComponent
      inputValue={inputValue}
      changeInputValue={changeInputValue}
      searchSubmit={searchSubmit}
      repositories={repositories}
      sortBy={sortBy}
      changeSortByType={changeSortByType}
      page={page}
      pagesCount={maxPagesCount}
      changePage={changePage}
      status={status}
    />
  );
};
