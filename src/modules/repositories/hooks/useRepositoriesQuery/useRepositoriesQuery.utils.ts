import camelcaseKeys from 'camelcase-keys';

import { IRepositories } from '../../repositories.types';

export const selectRepositoriesData = (data: IRepositories | undefined) => {
  if (!data) {
    return { items: [], totalCount: 0 };
  }

  const { items, totalCount } = camelcaseKeys(data);
  const repositories = camelcaseKeys(items).map(({ id, name, owner, createdAt, stargazersCount }) => ({
    id,
    owner: {
      login: owner.login,
    },
    name,
    createdAt,
    stargazersCount,
  }));

  return { items: repositories || [], totalCount };
};
