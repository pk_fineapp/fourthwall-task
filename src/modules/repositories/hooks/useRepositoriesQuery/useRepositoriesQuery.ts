import { useQuery } from 'react-query';
import axios from 'axios';
import { stringify } from 'query-string';
import snakecaseKeys from 'snakecase-keys';

import { useQueryParams } from '../../../../hooks/useQueryParams/useQueryParams';
import { BASE_URL } from '../../../../app.constants';
import { IRepositories } from '../../repositories.types';

import { selectRepositoriesData } from './useRepositoriesQuery.utils';

export const useRepositoriesQuery = () => {
  const { query } = useQueryParams();

  const repositoriesQuery = useQuery(
    ['repositories', query],
    async () => {
      if (BASE_URL) {
        const queryString = stringify(snakecaseKeys(query));
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const { data } = await axios.get(`${BASE_URL}?${queryString}`);

        return data as IRepositories;
      } else {
        throw new Error('Github API URL undefined');
      }
    },
    {
      enabled: query && !!query.q,
      refetchOnMount: false,
      refetchOnWindowFocus: false,
    }
  );

  return {
    repositories: selectRepositoriesData(repositoriesQuery.data),
    repositoriesQuery,
  };
};
