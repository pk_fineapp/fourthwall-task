import { ITableColumn } from '../../components/table/tableHead/tableHead.types';

export const repositoriesTableColumns: ITableColumn[] = [
  { value: 'Name', width: 35 },
  { value: 'Owner', width: 30 },
  { value: 'Stars', width: 12 },
  { value: 'Created at', width: 23 },
];