import React from 'react';

import { SearchBar } from '../../components/searchBar';

import { Container } from './repositories.styles';
import { IRepositoriesComponent } from './repositories.types';
import { RepositoriesContent } from './components/repositoriesContent';

export const RepositoriesComponent = ({
  inputValue,
  changeInputValue,
  searchSubmit,
  repositories,
  status,
  changeSortByType,
  page,
  pagesCount,
  changePage,
  sortBy,
}: IRepositoriesComponent) => {
  return (
    <Container data-testid="repositories-component">
      <SearchBar
        value={inputValue}
        changeValue={changeInputValue}
        inputPlaceholder="Search github repositories"
        submit={searchSubmit}
      />
      <RepositoriesContent
        repositories={repositories}
        status={status}
        changeSortByType={changeSortByType}
        sortBy={sortBy}
        page={page}
        pagesCount={pagesCount}
        changePage={changePage}
      />
    </Container>
  );
};
