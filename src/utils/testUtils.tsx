import React, { ReactNode } from 'react';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';
import { Router, Route } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';
import { QueryClient, QueryClientProvider } from 'react-query';

export const getMeta = (metaName: string) => {
  const metas = document.getElementsByTagName('meta');
  for (let i = 0; i < metas.length; i += 1) {
    if (metas[i].getAttribute('name') === metaName) {
      return metas[i].getAttribute('content');
    }
  }

  return '';
};

export const wrapRender = (ui: ReactNode, initialRoute = '') => {
  const queryClient = new QueryClient();

  const history = createMemoryHistory({ initialEntries: [initialRoute] });
  return {
    ...render(
      <Router history={history}>
        <QueryParamProvider ReactRouterRoute={Route}>
          <QueryClientProvider client={queryClient}>{ui}</QueryClientProvider>
        </QueryParamProvider>
      </Router>
    ),
    history,
  };
};
