import React from 'react';
import { waitFor } from '@testing-library/react';

import App from './app.component';
import { getMeta, wrapRender } from './utils/testUtils';

describe('App', () => {
  const render = () => wrapRender(<App />);

  test('should display proper content', () => {
    const { getByTestId } = render();

    expect(getByTestId('background')).toBeInTheDocument();
    expect(getByTestId('repositories-component')).toBeInTheDocument();
  });

  test('should set proper title and description', async () => {
    render();

    await waitFor(() => expect(document.title).toEqual('Fourthwall Task'));
    await waitFor(() => expect(getMeta('description')).toEqual('Fourthwall Task'));
  });
});
