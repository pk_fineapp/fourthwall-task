export const BASE_URL = process.env.REACT_APP_GITHUB_API_URL;

export const RESULTS_PER_PAGE_COUNT = 10;
export const MAX_PAGES = 100;