import React from 'react';
import { render } from '@testing-library/react';

import { Pagination } from '../pagination.component';
import { IPagination } from '../pagination.types';

describe('Pagination', () => {
  const onChange = jest.fn();
  const activePage = 1;
  const count = 5;

  const defaultProps = {
    id: 'pagination',
    page: 1,
    count,
    onChange,
  };

  const renderComponent = (props?: Partial<IPagination>) => render(<Pagination {...defaultProps} {...props} />);

  test('should display proper content', () => {
    const { getByTestId } = renderComponent();

    const pagination = getByTestId('pagination');
    const pages = pagination.querySelectorAll('li');
    const currentPage = pages[activePage].querySelector('button');

    expect(pagination).toBeInTheDocument();
    expect(pages).toHaveLength(count + 2);
    expect(currentPage).toHaveAttribute('aria-current', 'true')
  });

  test('should call on Change when next page is chosen', () => {
    const { getByTestId } = renderComponent();

    const pagination = getByTestId('pagination');
    const pages = pagination.querySelectorAll('li');
    const nextPage = pages[activePage + 1].querySelector('button') as HTMLButtonElement;
    
    nextPage.click();

    expect(onChange).toBeCalledWith(2);
  });
});
