export interface IPagination {
  id: string;
  count: number;
  page: number;
  onChange: (page: number) => void;
}