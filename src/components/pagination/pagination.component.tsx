import React, { ChangeEvent } from 'react';
import { Pagination as PaginationComponent } from '@mui/material';

import { Container } from './pagination.styles';
import { IPagination } from './pagination.types';

export const Pagination = ({ id, count, page, onChange }: IPagination) => {
  const handleChange = (e: ChangeEvent<unknown>, page: number) => onChange(page);

  return (
    <Container>
      <PaginationComponent data-testid={id} count={count} page={page} onChange={handleChange} />
    </Container>
  );
};
