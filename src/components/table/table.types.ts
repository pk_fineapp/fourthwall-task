import { ReactNode } from 'react';

import { ITableColumn } from './tableHead/tableHead.types';

export interface ITable {
  columns: ITableColumn[];
  body: ReactNode;
}
