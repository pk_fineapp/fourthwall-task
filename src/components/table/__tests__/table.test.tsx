import React from 'react';
import { render } from '@testing-library/react';

import { Table } from '../table.component';
import { ITable } from '../table.types';
import { TableBody } from '../tableBody';

describe('Table', () => {
  const columns = [
    {
      value: 'column1',
    },
    {
      value: 'column2',
    },
  ];

  const body = (
    <TableBody>
      <tr data-testid="table-row1">
        <td data-testid="table-data1">cell1</td>
      </tr>
      <tr data-testid="table-row2">
        <td data-testid="table-data2">cell2</td>
      </tr>
    </TableBody>
  );

  const defaultProps = {
    columns,
    body,
  };

  const renderComponent = (props?: Partial<ITable>) => render(<Table {...defaultProps} {...props} />);

  test('display proper content', () => {
    const { getByTitle, getByTestId } = renderComponent();

    const tableHead = document.querySelector('thead')
    const tableBody = document.querySelector('tbody')
    expect(tableHead).toBeInTheDocument();
    expect(tableHead).toContainElement(getByTitle('column1'));
    expect(tableHead).toContainElement(getByTitle('column2'));
    expect(tableBody).toBeInTheDocument();
    expect(tableBody).toContainElement(getByTestId('table-row1'));
    expect(tableBody).toContainElement(getByTestId('table-row2'));
  });
});
