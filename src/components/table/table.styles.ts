import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  table: {
    height: '100%',
    width: '640px',
    background: 'none',
    tableLayout: 'fixed',
    '& td': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      cursor: 'default',
    },
  },
  tableContainer: {
    background: 'none',
    boxShadow: 'none',
  },
});