import React from 'react';
import { render } from '@testing-library/react';

import { TableBody } from '../tableBody.component';
import { ITableBody } from '../tableBody.types';

describe('TableBody', () => {
  const defaultProps = {
    children: (
      <tr data-testid="table-row">
        <td data-testid="table-data">cell</td>
      </tr>
    ),
  };

  const renderComponent = (props?: Partial<ITableBody>) =>
    render(
      <table>
        <TableBody {...defaultProps} {...props} />
      </table>
    );

  test('display proper content', () => {
    const { getByTestId } = renderComponent();

    const tableRow = getByTestId('table-row');
    const tableCell= getByTestId('table-data');

    expect(tableRow).toBeInTheDocument();
    expect(tableCell).toBeInTheDocument();
    expect(tableCell).toHaveTextContent('cell');
  });
});
