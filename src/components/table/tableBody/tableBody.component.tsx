import React from 'react';
import TableBodyComponent from '@material-ui/core/TableBody';

import { ITableBody } from './tableBody.types';

export const TableBody = ({ children }: ITableBody) => {
  return <TableBodyComponent>{children}</TableBodyComponent>
};
