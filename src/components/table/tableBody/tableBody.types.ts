import { ReactNode } from 'react';

export interface ITableBody {
  children: ReactNode;
}
