import React from 'react';
import { render } from '@testing-library/react';

import { TableRow } from '../tableRow.component';
import { ITableRow } from '../tableRow.types';

describe('TableCell', () => {
  const defaultProps = {
    children: (
      <>
        <td data-testid="table-data1">cell1</td>
        <td data-testid="table-data2">cell2</td>
      </>
    ),
  };

  const renderComponent = (props?: Partial<ITableRow>) =>
    render(
      <table>
        <tbody>
          <TableRow {...defaultProps} {...props} />
        </tbody>
      </table>
    );

  test('display proper content', () => {
    const { getByTestId } = renderComponent();

    const tableCell1 = getByTestId('table-data1');
    const tableCell2 = getByTestId('table-data2');
    
    expect(tableCell1).toBeInTheDocument();
    expect(tableCell1).toHaveTextContent('cell1');
    expect(tableCell2).toBeInTheDocument();
    expect(tableCell2).toHaveTextContent('cell2');
  });
});
