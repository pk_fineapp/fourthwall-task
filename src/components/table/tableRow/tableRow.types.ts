import { ReactNode } from 'react';

export interface ITableRow {
  children: ReactNode;
}
