import React from 'react';
import TableRowComponent from '@material-ui/core/TableRow';

import { ITableRow } from './tableRow.types';

export const TableRow = ({ children }: ITableRow) => {
  return <TableRowComponent>{children}</TableRowComponent>
};
