import React from 'react';
import TableContainer from '@material-ui/core/TableContainer';
import TableComponent from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper/Paper';

import { useStyles } from './table.styles';
import { ITable } from './table.types';
import { TableHead } from './tableHead';

export const Table = ({ columns, body }: ITable) => {
  const classes = useStyles();

  return (
    <TableContainer className={classes.tableContainer} component={Paper}>
      <TableComponent className={classes.table}>
        <TableHead columns={columns} />
        {body}
      </TableComponent>
    </TableContainer>
  );
};
