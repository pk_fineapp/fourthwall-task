import React from 'react';
import { render } from '@testing-library/react';

import { TableCell } from '../tableCell.component';
import { ITableCell } from '../tableCell.types';

describe('TableCell', () => {
  const width = 20;
  
  const defaultProps = {
    value: 'test',
    width,
  };

  const renderComponent = (props?: Partial<ITableCell>) =>
    render(
      <table>
        <tbody>
          <tr>
            <TableCell {...defaultProps} {...props} />
          </tr>
        </tbody>
      </table>
    );

  test('display proper content', () => {
    const { getByTitle } = renderComponent();

    const tableCell = getByTitle('test');
    expect(tableCell).toHaveTextContent('test');
    expect(tableCell).toHaveAttribute('width', `${width}%`);
  });
});
