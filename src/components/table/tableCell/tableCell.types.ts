export type AlignType = 'left' | 'center' | 'right';

export interface ITableCell {
  value: string;
  width?: number;
  head?: boolean;
  align?: AlignType;
}