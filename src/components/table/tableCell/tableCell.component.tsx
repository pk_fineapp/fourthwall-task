import React from 'react';
import TableCellComponent from '@material-ui/core/TableCell';

import { ITableCell } from './tableCell.types';
import { Cell } from './tableCell.styles';

export const TableCell = ({ value, width, align = 'left', head = false }: ITableCell) => {
  return (
    <TableCellComponent align={align} title={value} width={width ? `${width}%` : undefined}>
      <Cell head={head}>{value}</Cell>
    </TableCellComponent>
  )
};
