import styled, { css } from 'styled-components';

const headCellStyles = css`
  font-weight: 600;
  position: relative;
  padding-right: 16px;
  width: fit-content;
`;

export const Cell = styled.div<{ head: boolean }>`
  white-space: nowrap;
  
  ${({ head }) => head ? headCellStyles : null};
`;