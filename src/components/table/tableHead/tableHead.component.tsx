import React from 'react';
import TableHeadComponent from '@material-ui/core/TableHead';

import { TableRow } from '../tableRow/tableRow.component';
import { TableCell } from '../tableCell';

import { ITableColumn, ITableHead } from './tableHead.types';

export const TableHead = ({ columns }: ITableHead) => {
  return (
    <TableHeadComponent>
      <TableRow>
        {columns.map((column: ITableColumn) => (
          <TableCell key={column.value} value={column.value} align={column.align} width={column.width} head />
        ))}
      </TableRow>
    </TableHeadComponent>
  );
};
