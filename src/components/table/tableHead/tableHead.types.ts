import { AlignType } from '../tableCell/tableCell.types';

export interface ITableColumn {
  value: string;
  align?: AlignType;
  width?: number;
}

export interface ITableHead {
  columns: ITableColumn[];
}
