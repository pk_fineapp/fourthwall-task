import React from 'react';
import { render } from '@testing-library/react';

import { TableHead } from '../tableHead.component';
import { ITableHead } from '../tableHead.types';

describe('TableHead', () => {
  const columns = [
    {
      value: 'column1',
    },
    {
      value: 'column2',
    },
  ];

  const defaultProps = {
    columns,
  };

  const renderComponent = (props?: Partial<ITableHead>) =>
    render(
      <table>
        <TableHead {...defaultProps} {...props} />
      </table>
    );

  test('display proper columns', () => {
    const { getByTitle } = renderComponent();

    expect(getByTitle('column1')).toBeInTheDocument();
    expect(getByTitle('column2')).toBeInTheDocument();
  });
});
