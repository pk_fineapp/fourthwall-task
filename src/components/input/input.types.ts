export interface IInput {
  value: string;
  placeholder?: string;
  onChange: (v: string) => void;
  onEnterKey?: () => void;
  noBorder?: boolean;
}