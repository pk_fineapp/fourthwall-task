import React, { ChangeEvent, KeyboardEvent } from 'react';

import { Container } from './input.styles';
import { IInput } from './input.types';

export const Input = ({ value, placeholder, onChange, onEnterKey, noBorder = false }: IInput) => {
  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => onChange(event.target.value);
  const handleOnKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      onEnterKey && onEnterKey();
    }
  };

  return (
    <Container
      type="text"
      value={value}
      placeholder={placeholder || ''}
      onChange={handleOnChange}
      onKeyDown={handleOnKeyDown}
      noBorder={noBorder}
    />
  );
};
