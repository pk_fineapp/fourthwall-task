import styled from 'styled-components';

import { COLORS } from '../../theme/colors';

export const Container = styled.input<{ noBorder?: boolean }>`
  height: 42px;
  width: 100%;
  align-self: stretch;
  padding: 0 32px 0 16px;
  margin: 0;
  border: 1px solid ${COLORS.GRAY_LIGHT};
  font-size: 14px;
  background: ${COLORS.WHITE};
  
  &:focus {
    outline: none;
  }
  
  &::placeholder {
    color: ${COLORS.GRAY};
  }
  
  ${({ noBorder }) => noBorder ? 'border: none': ''};
`;
