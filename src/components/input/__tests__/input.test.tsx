import React from 'react';
import {fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { Input } from '../input.component';
import { IInput } from '../input.types';

describe('Input', () => {
  const onChange = jest.fn();
  const onEnterKey = jest.fn();

  const defaultProps = {
    value: 'text',
    placeholder: 'placeholder text',
    onChange,
    onEnterKey,
  };

  const renderComponent = (props?: Partial<IInput>) => render(<Input {...defaultProps} {...props} />);

  test('should display proper content', () => {
    const { getByPlaceholderText } = renderComponent();

    const input = getByPlaceholderText(/placeholder text/i);
    expect(input).toHaveAttribute('type', 'text');
    expect(input).toHaveValue('text');
  });

  test('should call onChange when value is changed', () => {
    const { getByPlaceholderText } = renderComponent();

    const input = getByPlaceholderText(/placeholder text/i);

    userEvent.type(input, 'x');

    expect(onChange).toBeCalledWith('textx');
  });

  test('should call onKeyEnter when enter is pressed', () => {
    const { getByPlaceholderText } = renderComponent();

    const input = getByPlaceholderText(/placeholder text/i);

    fireEvent.keyDown(input, {key: 'Enter', code: 'Enter'});

    expect(onEnterKey).toBeCalled();
  });
});
