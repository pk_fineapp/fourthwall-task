import styled from 'styled-components';
import { makeStyles } from '@material-ui/core';

import { COLORS } from '../../theme/colors';

export const useStyles = makeStyles(() => ({
  select: {
    fontSize: '14px',
    '&:before': {
      borderColor: COLORS.GRAY,
    },
    '& svg': {
      color: COLORS.GRAY,
    },
    '&:after': {
      borderColor: COLORS.BLUE,
    },
  },
  menuItem: {
    fontSize: '14px',
  },
}));

export const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;
