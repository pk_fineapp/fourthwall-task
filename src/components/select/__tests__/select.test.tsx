import React from 'react';
import { act, fireEvent, render } from '@testing-library/react';

import { Select } from '../select.component';
import { ISelect } from '../select.types';

describe('Select', () => {
  const onChange = jest.fn();
  const items = ['test1', 'test2', 'test3'];

  const defaultProps = {
    id: 'select',
    value: 'test1',
    items,
    onChange,
  };

  const renderComponent = (props?: Partial<ISelect>) => render(<Select {...defaultProps} {...props} />);

  test('should display proper content', () => {
    renderComponent();

    const select = document.querySelector('#select');
    const input = document.querySelector('input');

    expect(select).toHaveTextContent('test1');
    expect(input).toHaveValue('test1');
  });

  test('should display items to select when clicked', () => {
    const { getByRole, getAllByRole } = renderComponent();

    const select = document.querySelector('#select') as HTMLButtonElement;
    fireEvent.mouseDown(select);

    const listbox = getByRole('listbox');
    const options = getAllByRole('option');

    expect(listbox).toBeInTheDocument();
    expect(options).toHaveLength(3);
    options.forEach((option, index) => expect(option).toHaveTextContent(items[index]));
  });

  test('should call onChange when value is changed', () => {
    const { getAllByRole } = renderComponent();

    const select = document.querySelector('#select') as HTMLButtonElement;
    fireEvent.mouseDown(select);

    act(() => {
      const options = getAllByRole('option');
      fireEvent.mouseDown(options[1]);
      options[1].click();
    });

    expect(onChange).toBeCalledWith('test2');
  });
});
