import React, { ChangeEvent } from 'react';
import SelectComponent from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { Container, useStyles } from './select.styles';
import { ISelect } from './select.types';

export const Select = ({ id, value, items, onChange }: ISelect) => {
  const classes = useStyles();

  const handleOnChange = (event: ChangeEvent<{ value: unknown }>) => {
    const { value } = event.target;

    onChange(value);
  };

  return (
    <Container>
      <SelectComponent id={id} value={value} className={classes.select} onChange={handleOnChange}>
        {items.map((item: string | number) => (
          <MenuItem key={item} className={classes.menuItem} value={item}>
            {item}
          </MenuItem>
        ))}
      </SelectComponent>
    </Container>
  );
};
