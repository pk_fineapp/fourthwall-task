export interface ISelect {
  id: string;
  value: string | number;
  items: (string | number)[];
  onChange: (v: unknown) => void;
}