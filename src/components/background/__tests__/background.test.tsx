import React from 'react';
import { render } from '@testing-library/react';

import { Background } from '../background.component';

describe('Background', () => {
  const renderComponent = () => render(<Background />);

  test('should display proper content', () => {
    const { getByTestId } = renderComponent();
    
    expect(getByTestId('background')).toBeInTheDocument();
    expect(getByTestId('face-logo')).toBeInTheDocument();
  });
});
