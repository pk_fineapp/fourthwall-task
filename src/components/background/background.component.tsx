import React from 'react';

import { Container, Face } from './background.styles';

export const Background = () => {
  return (
    <Container data-testid="background">
      <Face data-testid="face-logo" />
    </Container>
  );
};
