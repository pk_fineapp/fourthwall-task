import styled, { keyframes } from 'styled-components';

import { ReactComponent as faceSVG } from '../../assets/images/face.svg';

const rotateAnimation = keyframes`
  from {
    transform: translate(-50%, -50%) rotate(0deg);
  }
  to {
    transform: translate(-50%, -50%) rotate(360deg);
  }
`;

export const Container = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  z-index: -1;
`;


export const Face = styled(faceSVG)`
  position: absolute;
  height: 65vh;
  top: 50%;
  left: 50%;
  animation: ${rotateAnimation} 20s linear infinite;
  opacity: 0.01;
`;
