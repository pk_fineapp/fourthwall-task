import React from 'react';

import { Button } from '../button';
import { Input } from '../input';

import { Container } from './searchBar.styles';
import { ISearchBar } from './searchBar.types';

export const SearchBar = ({ value, changeValue, inputPlaceholder, submit }: ISearchBar) => {
  return (
    <Container>
      <Input value={value} placeholder={inputPlaceholder} onChange={changeValue} onEnterKey={submit} noBorder />
      <Button
        onClick={submit}
      >
        Submit
      </Button>
    </Container>
  );
};
