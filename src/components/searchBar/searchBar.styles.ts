import styled from 'styled-components';

import { COLORS } from '../../theme/colors';


export const Container = styled.div`
  height: 42px;
  width: 640px;
  display: flex;
  box-shadow: 3px 3px 5px 3px rgba(0, 0, 0, 0.05);
  background: ${COLORS.WHITE};
`;
