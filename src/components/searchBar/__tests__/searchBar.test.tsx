import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { SearchBar } from '../searchBar.component';
import { ISearchBar } from '../searchBar.types';

describe('SearchBar', () => {
  const changeValue = jest.fn();
  const submit = jest.fn();

  const defaultProps = {
    value: 'text',
    inputPlaceholder: 'input placeholder',
    changeValue,
    submit,
  };

  const renderComponent = (props?: Partial<ISearchBar>) => render(<SearchBar {...defaultProps} {...props} />);

  test('should display proper content', () => {
    const { getByRole, getByPlaceholderText } = renderComponent();

    const button = getByRole('button');
    const input = getByPlaceholderText('input placeholder');
    expect(button).toBeInTheDocument();
    expect(input).toBeInTheDocument();
  });

  test('should pass proper value to input', () => {
    const { getByPlaceholderText } = renderComponent();

    const input = getByPlaceholderText('input placeholder');
    expect(input).toHaveValue('text');
  });

  test('should call changeValue when input value is changed', () => {
    const { getByPlaceholderText } = renderComponent();
    const input = getByPlaceholderText('input placeholder');

    userEvent.type(input, 'x');

    expect(changeValue).toBeCalledWith('textx');
  });

  test('should call submit when button is pressed', () => {
    const { getByRole } = renderComponent();
    
    const button = getByRole('button');
    fireEvent.click(button);
    
    expect(submit).toBeCalled();
  });

  test('should call submit when enter key is pressed while typing', () => {
    const { getByPlaceholderText } = renderComponent();

    const input = getByPlaceholderText('input placeholder');
    fireEvent.keyDown(input, {key: 'Enter', code: 'Enter'});

    expect(submit).toBeCalled();
  });
});
