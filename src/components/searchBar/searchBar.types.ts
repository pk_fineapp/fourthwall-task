export interface ISearchBar {
  value: string;
  changeValue: (v: string) => void;
  submit: () => void;
  inputPlaceholder?: string;
}
