import styled from 'styled-components';

import { COLORS } from '../../theme/colors';


export const Container = styled.button`
  height: 42px;
  padding: 0 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0;
  background-color: ${COLORS.BLUE};
  color: ${COLORS.WHITE};
  border: none;
  font-weight: bold;
  cursor: pointer;
  font-size: 14px;
  
  &:disabled {
    opacity: 0.5;
  }
  
  &:hover {
    background-color: ${COLORS.BLUE_HOVER};
    
    &:disabled {
      background-color: ${COLORS.BLUE};
    }
  }
  
  &:active {
    background-color: ${COLORS.BLUE_ACTIVE};
  }
`;
