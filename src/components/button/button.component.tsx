import React from 'react';

import { Container } from './button.styles';
import { IButton } from './button.types';

export const Button = ({ children, type = 'button', disabled = false, onClick }: IButton) => {
  return (
    <Container type={type} disabled={disabled} onClick={onClick}>
      {children}
    </Container>
  );
};