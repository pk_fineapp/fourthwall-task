import { ReactNode } from 'react';

export interface IButton {
  children: string | ReactNode;
  onClick: () => void;
  type?: 'submit' | 'reset' | 'button';
  disabled?: boolean;
}
