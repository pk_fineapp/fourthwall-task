import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import { Button } from '../button.component';
import { IButton } from '../button.types';

describe('Button', () => {
  const onClick = jest.fn();

  const defaultProps = {
    children: <span>button text</span>,
    onClick,
  };

  const renderComponent = (props?: Partial<IButton>) => render(<Button {...defaultProps} {...props} />);

  test('should display proper content', () => {
    const { getByRole } = renderComponent();

    const button = getByRole('button');
    expect(button).toHaveTextContent(/button text/i);
    expect(button).toHaveAttribute('type', 'button');
    expect(button).not.toHaveAttribute('disabled');
  });

  test('should have proper type when type prop is provided', () => {
    const { getByRole } = renderComponent({ type: 'submit' });

    const button = getByRole('button');
    expect(button).toHaveAttribute('type', 'submit');
  });

  test('should be disabled when disabled prop is true', () => {
    const { getByRole } = renderComponent({ disabled: true });

    const button = getByRole('button');
    expect(button).toHaveAttribute('disabled');
  });

  test('should call onClick prop on click', () => {
    const { getByRole } = renderComponent();

    const button = getByRole('button');
    fireEvent.click(button);
    expect(onClick).toBeCalled();
  });
});
