#Fourthwall Task - Przemysław Kwiatkowski


## About project


 ### How it's made
 
I did this for several days, piece by piece. I didn't count the time, but I assume everything you see took me about 12-15h.  

 Step by step:
 - started with checking Github Search API using Postman
 - designed the interface on Figma referring to Fourthwall theme (colors, font, button, etc)
 - created a cra app, added libraries
 - implemented layout and components based on design
 - added logic and adjustments
 - covered part of the app with unit tests
 
Typically, in commercial projects, the order of these steps varies somewhat. Implementing components is usually followed 
by unit tests and styles are polished at the end. Here, however, due to the size of the app and designing it so I knew 
what I want to achieve, the order is a little bit different. I made components and styles, I worked with logic and I left unit tests as last thing to do, as it is usually time-consuming and I did not want to 
spend whole time covering app with tests. 

  ### Features

 - [x] React & Typescript
  
   Started with [Create React App](https://github.com/facebook/create-react-app).
    
 - [x] Search input
 
  I decided to make a search input with submit button instead of applying search query while typing with some debounce. 
  This is because I checked few websites (even Google or Facebook) and stores and each of them sends a request after 
  submitting action and then sets URL search params. Moreover, Github API has its limit - it allows to make up to 10 
  requests per minute, so it would be easy to exceed with debounce solution.
    
 - [x] Results table with sortable columns: Name, Owner, Stars and Created at
 
   Material-UI is used to provide table and select components.
   According to [Docs](https://docs.github.com/en/rest/reference/search) sort queries are: *stars*, *forks*, 
   *help-wanted-issues* and *updated*. I did sorting by *stars* and it also worked with *name*. Sorting is similar to 
   what stores usually have - a select. 
   
 - [x] Pagination
    
   I did my own one, although it was not a good idea. I limited pages to 100 as Github was returning errors with larger 
   amount. 
   
 - [x] Nice and clean interface, including loading and error states.

   I hope it's nice and clean. I limited results to 10 to keep everything on one single page without scrolling. 
   If it goes about errors - it's showing only `Something went wrong` error to the user without specific code message 
   or code.
   
 - [x] Caching the search results
 
   I was thinking about react-query and also about saving it to ref or localStorage to keep it between sessions. 
   Eventually I decided to go with ref as I have never done it before. 
   
 - [x] Being able to see the exact same results for a given URL with search params
 
   It should work for single queries. I have not tested it with multi search params.
 
 - [x] Separating business and visual code

   I am not sure if you will be satisfied with this. I guess it is about to use more pure components based on props 
   while the logic should be implemented in parent components. I was thinking about optimizing it, but I recon I 
   spent enough time on the whole task.
   
 - [x] Testing your code (preferably Jest, Cypress),
 
   I implemented few unit tests (Jest + React Testing Library) for some components and hooks. It's not fully covered. 
   I wanted also to try Cypress to make one or two e2e tests, but it's also about time. 
   
 - [x] Type interfaces
 
 - [x] Useful git branch history
 
 - [x] Anything else you think might be useful here.
 
    Context API is used to keep global store.
    I was also thinking about moving hooks to specific folders where they are used, as they are not used anywhere else. 
    They are left in hooks folder though.
    
 ### Potential improvements
 
  - I would make the components more generic and pure (for example Input and Button could be pure components based on 
  props only and used in SearchBar) 
  - Caching is done by reference. I wanted to try doing it this way and it 
  seemed to be easy to implement. However, I would rather use react-query library as it's known for its caching 
  functionality and requests optimization.
  - Context API is used. I would probably move to Redux if the app would about to grow. 
  - All copies are hardcoded - I would use some internationalization library to keep copies in separated files, easy to 
  change, and ready for adding other languages
  - Material-UI causes some errors in the console. I would investigate it or try to implement custom components instead
  - I would refine pagination or use ready-to-use pagination
  - Max 10 results are displayed now. I would add an option which would allow to select how many results should be 
  displayed in the table
  - Handle multi search params (arrays) in URL
  - I would add responsiveness or at least a view informing about too small screen.
  - Server-side errors could be shown as toasts. 
  - I would use Immer or other library to keep the store immutable

  
## Quick start

npm version:  **6.14.7**

yarn version:  **1.22.10**


1.  **Install dependencies**

    ```shell
    yarn install
    ```

2.  **Create env files**

    Create `.env.local` or `.env` base on the `.env.example`. No need to fufill anything.

3.  **Start developing**

    Open your development server by running:

    ```shell
    yarn start
    ```
    It runs the app in the development mode.\
    Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Other scripts to use

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.